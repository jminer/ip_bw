/*
 *  Copyright 2018 Jordan Miner
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

// https://stackoverflow.com/questions/29553990/print-tcp-packet-data
// https://netfilter.org/documentation/HOWTO//netfilter-hacking-HOWTO-3.html
// https://kernelnewbies.org/FAQ/Hashtables
// http://vger.kernel.org/~davem/skb_data.html
// https://stackoverflow.com/questions/8832114/what-does-init-mean-in-the-linux-kernel-code

#include <linux/fs.h>
#include <linux/hashtable.h>
#include <linux/in.h>
#include <linux/in6.h>
#include <linux/ip.h>
#include <linux/ipv6.h>
#include <linux/netfilter.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/skbuff.h>
#include <linux/slab.h>
#include <linux/types.h>
#include <net/ipv6.h>

#define MAX_IP_ADDRS	255

struct ip_bw_record {
	__be32		ip_addr;
	struct in6_addr	ip6_addr;
	/* bytes transferred from the IP address to another on the LAN */
	__u64		lan_tx;
	/* bytes transferred to the IP address from another on the LAN */
	__u64		lan_rx;
	/* bytes transferred from the IP address to the WAN */
	__u64		wan_tx;
	/* bytes transferred to the IP address to the WAN */
	__u64		wan_rx;

	struct hlist_node node;
};

static DEFINE_HASHTABLE(record_table, 4);

static __u64 uncounted_bw = 0;

static DEFINE_SPINLOCK(records_lock);

static struct nf_hook_ops ip_ops;
static struct nf_hook_ops ip6_ops;

static bool is_ip_local(__u32 ip_addr)
{
	// 10.0.0.0/8
	// 172.16.0.0/12
	// 192.168.0.0/16
	return ipv4_is_private_10(ip_addr) ||
		ipv4_is_private_172(ip_addr) ||
		ipv4_is_private_192(ip_addr);
}

struct ip_bw_record *get_ip_record(__u32 ip_addr)
{
	struct ip_bw_record *record;
	hash_for_each_possible(record_table, record, node, ip_addr) {
		if (record->ip_addr == ip_addr) {
			return record;
		}
	}
	return NULL;
}

static __u64 ip6_addr_key(struct in6_addr *ip6_addr)
{
	__u64 lower = ((__u64)ip6_addr->s6_addr32[1] << 32) | ip6_addr->s6_addr32[0];
	__u64 higher = ((__u64)ip6_addr->s6_addr32[3] << 32) | ip6_addr->s6_addr32[2];
	return higher ^ hash_64(lower, 32);
}

struct ip_bw_record *get_ip6_record(struct in6_addr ip6_addr)
{
	struct ip_bw_record *record;
	__u64 ip_key = ip6_addr_key(&ip6_addr);
	hash_for_each_possible(record_table, record, node, ip_key) {
		if (memcmp(&record->ip6_addr, &ip6_addr, sizeof ip6_addr) == 0) {
			return record;
		}
	}
	return NULL;
}

void add_bandwidth(__be32 saddr, __be32 daddr, unsigned int count)
{
	if(ipv4_is_loopback(saddr) || ipv4_is_loopback(daddr))
		return;
	bool is_src_local = is_ip_local(saddr);
	bool is_dest_local = is_ip_local(daddr);
	//if(!is_src_local)
	//	printk("ip_bw: non-local IP address %pI4", &saddr);
	//if(!is_dest_local)
	//	printk("ip_bw: non-local IP address %pI4", &daddr);

	unsigned long flags;
	spin_lock_irqsave(&records_lock, flags);
	if (is_src_local) {
		struct ip_bw_record *src_ip_rec = get_ip_record(saddr);
		if (!src_ip_rec) {
			src_ip_rec = kzalloc(sizeof(struct ip_bw_record), GFP_ATOMIC);
			src_ip_rec->ip_addr = saddr;
			hash_add(record_table, &src_ip_rec->node, saddr);
			printk("ip_bw: adding IP address %pI4", &saddr);
		}
		if (src_ip_rec) {
			if (is_dest_local) {
				src_ip_rec->lan_tx += count;
			} else {
				src_ip_rec->wan_tx += count;
			}
		} else {
			uncounted_bw += count;
		}
	}
	if (is_dest_local) {
		struct ip_bw_record *dest_ip_rec = get_ip_record(daddr);
		if (!dest_ip_rec) {
			dest_ip_rec = kzalloc(sizeof(struct ip_bw_record), GFP_ATOMIC);
			dest_ip_rec->ip_addr = daddr;
			hash_add(record_table, &dest_ip_rec->node, daddr);
			printk("ip_bw: adding IP address %pI4", &daddr);
		}
		if (dest_ip_rec) {
			if (is_src_local) {
				dest_ip_rec->lan_rx += count;
			} else {
				dest_ip_rec->wan_rx += count;
			}
		} else {
			uncounted_bw += count;
		}
	}
	spin_unlock_irqrestore(&records_lock, flags);
}

// TODO: Checking if an IPv6 address is link-local may not be good enough. There
// is no NAT with IPv6, so every computer in a home will have an IP address on
// the internet. Packets addressed with a link-local address can't be routed
// outside the network, but I think packets can use the internet address
// assigned to each computer to send packets within the LAN. I may need to check
// which network interface the packet is routed across. I think I could use the
// `state->in` and `state->out` fields. In the pre-routing hook, it seems the
// `out` `net_device` is not set yet, but later hooks would probably have it.
// The net_device has a `name` field that could searched for "wlan".
void add_bandwidth6(struct in6_addr saddr, struct in6_addr daddr, unsigned int count)
{
	int src_scope = ipv6_addr_scope(&saddr);
	int dest_scope = ipv6_addr_scope(&daddr);
	if((src_scope & IPV6_ADDR_LOOPBACK) || (dest_scope & IPV6_ADDR_LOOPBACK))
		return;
	bool is_src_local = src_scope == IPV6_ADDR_SCOPE_LINKLOCAL;
	bool is_dest_local = dest_scope == IPV6_ADDR_SCOPE_LINKLOCAL;
	//if(!is_src_local)
	//	printk("ip_bw: non-local IP address %pI6", &saddr);
	//if(!is_dest_local)
	//	printk("ip_bw: non-local IP address %pI6", &daddr);

	unsigned long flags;
	spin_lock_irqsave(&records_lock, flags);
	if (is_src_local) {
		struct ip_bw_record *src_ip_rec = get_ip6_record(saddr);
		if (!src_ip_rec) {
			src_ip_rec = kzalloc(sizeof(struct ip_bw_record), GFP_ATOMIC);
			src_ip_rec->ip6_addr = saddr;
			__u64 ip_key = ip6_addr_key(&saddr);
			hash_add(record_table, &src_ip_rec->node, ip_key);
			printk("ip_bw: adding IPv6 address %pI6", &saddr);
		}
		if (src_ip_rec) {
			if (is_dest_local) {
				src_ip_rec->lan_tx += count;
			} else {
				src_ip_rec->wan_tx += count;
			}
		} else {
			uncounted_bw += count;
		}
	}
	if (is_dest_local) {
		struct ip_bw_record *dest_ip_rec = get_ip6_record(daddr);
		if (!dest_ip_rec) {
			dest_ip_rec = kzalloc(sizeof(struct ip_bw_record), GFP_ATOMIC);
			dest_ip_rec->ip6_addr = daddr;
			__u64 ip_key = ip6_addr_key(&daddr);
			hash_add(record_table, &dest_ip_rec->node, ip_key);
			printk("ip_bw: adding IPv6 address %pI6", &daddr);
		}
		if (dest_ip_rec) {
			if (is_src_local) {
				dest_ip_rec->lan_rx += count;
			} else {
				dest_ip_rec->wan_rx += count;
			}
		} else {
			uncounted_bw += count;
		}
	}
	spin_unlock_irqrestore(&records_lock, flags);
}

static unsigned int ip_bw_ip_packet_hook(void *priv,
		    struct sk_buff *skb,
		    const struct nf_hook_state *state)
{
	unsigned int len = skb->len;
	struct iphdr *hdr = ip_hdr(skb);
	//printk("in_name: %s, out_name: %s", state->in ? state->in->name : "",
	//	state->out ? state->out->name : "");
	add_bandwidth(hdr->saddr, hdr->daddr, len);
	return NF_ACCEPT;
}

static unsigned int ip_bw_ip6_packet_hook(void *priv,
		    struct sk_buff *skb,
		    const struct nf_hook_state *state)
{
	unsigned int len = skb->len;
	struct ipv6hdr *hdr = ipv6_hdr(skb);
	//printk("in_name: %s, out_name: %s", state->in ? state->in->name : "",
	//	state->out ? state->out->name : "");
	add_bandwidth6(hdr->saddr, hdr->daddr, len);
	return NF_ACCEPT;
}

struct seq_iter {
	size_t bkt;
	struct hlist_node *current_node;
};

static unsigned long spin_lock_seq_flags;

static void *ip_bw_seq_start(struct seq_file *s, loff_t *pos)
{
	spin_lock_irqsave(&records_lock, spin_lock_seq_flags);
	loff_t skipped = 0;
	size_t bkt;

#if 0
	for (bkt = 0; bkt < HASH_SIZE(record_table); ++bkt) {
		if(record_table[bkt].first)
		printk("ip_bw: ip_bw_seq_start: table[%d].first: %08x, next: %08x",
			bkt, (u32)record_table[bkt].first,
			(u32)record_table[bkt].first->next);
		else
		printk("ip_bw: ip_bw_seq_start: table[%d].first: %08x",
			bkt, (u32)record_table[bkt].first);
	}
#endif

	for (bkt = 0; bkt < HASH_SIZE(record_table); ++bkt) {
		struct ip_bw_record *record;
		hlist_for_each_entry(record, &record_table[bkt], node) {
			if (skipped++ == *pos) {
				struct seq_iter *iter =
					kmalloc(sizeof(struct seq_iter), GFP_KERNEL);
				iter->bkt = bkt;
				iter->current_node = &record->node;
				return iter;
			}
		}
	}

	return NULL;
}

static void *ip_bw_seq_next(struct seq_file *s, void *v, loff_t *pos)
{
	struct seq_iter *iter = v;
	struct hlist_node *next = iter->current_node->next;
	size_t bkt = iter->bkt;
	while (next == NULL) {
		++bkt;
		if (bkt >= HASH_SIZE(record_table))
			break;
		next = record_table[bkt].first;
	}
	//printk("ip_bw: ip_bw_seq_next, cur_node: %08x, next: %08x",
	//	iter->current_node, next);
	iter->bkt = bkt;
	iter->current_node = next;
	*pos += 1;
	return next == NULL ? NULL : iter;
}

static void ip_bw_seq_stop(struct seq_file *s, void *v)
{
	kfree(v);
	spin_unlock_irqrestore(&records_lock, spin_lock_seq_flags);
}

static int ip_bw_seq_show(struct seq_file *s, void *v)
{
	struct seq_iter *iter = v;
	struct ip_bw_record *record =
		container_of(iter->current_node, struct ip_bw_record, node);

#if 1
	if (record->ip_addr != 0) {
		seq_printf(s, "%pI4", &record->ip_addr);
	} else {
		seq_printf(s, "%pI6", &record->ip6_addr);
	}
	seq_printf(s, "  lan_tx/rx: %llu/%llu", record->lan_tx, record->lan_rx);
	seq_printf(s, "  wan_tx/rx: %llu/%llu", record->wan_tx, record->wan_rx);
	seq_printf(s, "\n");
#else
	if (record->ip_addr != 0) {
		seq_putc(s, 47); // IPv4 record
		seq_write(s, record->ip_addr, sizeof record->ip_addr);
	} else {
		seq_putc(s, 48); // IPv6 record
		seq_write(s, record->ip6_addr, sizeof record->ip6_addr);
	}
	seq_write(s, record->lan_tx, sizeof record->lan_tx);
	seq_write(s, record->lan_rx, sizeof record->lan_rx);
	seq_write(s, record->wan_tx, sizeof record->wan_tx);
	seq_write(s, record->wan_rx, sizeof record->wan_rx);
#endif
	return 0;
}

static const struct seq_operations ip_bw_seq_ops = {
	.start	= ip_bw_seq_start,
	.next	= ip_bw_seq_next,
	.stop	= ip_bw_seq_stop,
	.show	= ip_bw_seq_show,
};

static int ip_bw_proc_open(struct inode *inode, struct file *file)
{
	return seq_open(file, &ip_bw_seq_ops);
}

static const struct file_operations ip_bw_proc_fops = {
	.owner		= THIS_MODULE,
	.open		= ip_bw_proc_open,
	.read		= seq_read,
	.llseek		= seq_lseek,
	.release	= seq_release
};

static struct proc_dir_entry *proc_entry;

static int __init ip_bw_init(void)
{
	int res;

	ip_ops.hook = ip_bw_ip_packet_hook;
	ip_ops.pf = PF_INET;
	ip_ops.hooknum = NF_INET_PRE_ROUTING;
	res = nf_register_hook(&ip_ops);
	if (res < 0) {
		pr_err("ip_bw: error in ipv4 nf_register_hook()\n");
		goto out1;
	}

	ip6_ops.hook = ip_bw_ip6_packet_hook;
	ip6_ops.pf = PF_INET6;
	ip6_ops.hooknum = NF_INET_PRE_ROUTING;
	res = nf_register_hook(&ip6_ops);
	if (res < 0) {
		pr_err("ip_bw: error in ipv6 nf_register_hook()\n");
		goto out2;
	}

	proc_entry = proc_create("ip_bw", 0, NULL, &ip_bw_proc_fops);
	if (!proc_entry) {
		res = -EIO;
		goto out3;
	}
	goto out1;

out3:
	nf_unregister_hook(&ip6_ops);
out2:
	nf_unregister_hook(&ip_ops);
out1:
	return res;
}

static void __exit ip_bw_exit(void)
{
	proc_remove(proc_entry);

	nf_unregister_hook(&ip_ops);
	nf_unregister_hook(&ip6_ops);

	unsigned long flags;
	spin_lock_irqsave(&records_lock, flags);
	size_t bkt;
	struct ip_bw_record *record;
	hash_for_each(record_table, bkt, record, node) {
		kfree(record);
	}
	spin_unlock_irqrestore(&records_lock, flags);
}

module_init(ip_bw_init);
module_exit(ip_bw_exit);

MODULE_AUTHOR("Jordan Miner");
MODULE_DESCRIPTION("Module to track bandwidth usage by IP address");
MODULE_LICENSE("GPL");
