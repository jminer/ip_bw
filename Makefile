
# https://www.kernel.org/doc/Documentation/kbuild/modules.txt

ifneq ($(KERNELRELEASE),)
# kbuild part of makefile
obj-m := ip_bw.o
#ip_bw-y := ip_bw.o

ccflags-y += -Wno-declaration-after-statement

else
# normal makefile
KDIR ?= /lib/modules/`uname -r`/build

default:
	$(MAKE) -C $(KDIR) M=$$PWD

endif
